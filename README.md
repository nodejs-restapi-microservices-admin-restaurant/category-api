# Documentation
* [CategoryMenu API](#category-menu-api)

## Category Menu API
### GET /categoryMenus
Returns all CategoryMenu instance.

Example response:
```json
[
    {
        "_id": "5dd0a8c6084bb372ff2e619b",
        "title": "Салаты",
        "description": "",
        "__v": 0
    },
    {
        "_id": "5dd19ed91c9d440000c0d802",
        "title": "Десерты",
        "description": "мммм",
        "__v": 0
    }
]
```

### POST /categoryMenus
Adds to CategoryMenu instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|description|`string`|

Example request:
```json
{
    "title": "Десерты",
    "description": "мммм"
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /categoryMenus/:id
Returns a CategoryMenu instance by id.

Example response:
```json
{
    "_id": "5dd0a8c6084bb372ff2e619b",
    "title": "Салаты",
    "description": "",
    "__v": 0
}
```
### PUT /categoryMenus/:id
Updates to CategoryMenu instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|description|`string`|

Example request:
```json
{
    "title": "Салаты",
    "description": ""
}
```
Example response:
```json
{
    "message": "Successfully updated!"
}
```
### DELETE /categoryMenus/:id
Delete a CategoryMenu instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
